---
layout: post
date: 2017-10-25 22:21:05 +01:00
title: "Linux as a language learning enviroment"

comments: true
categories: language-learning
tags: 
---

This article is a working in progress about the tools used to learn languages
using linux. Through the years I have had many different setups and many tools
that I don't use anymore, but they will referenced as well. If you're here I 
assume you already have an opnion about Linux and why you should or not use it.

Anyway, let's start by thinking how exactely would a operating system help us
with our language learning journey. Here is a tiny a list:


* Support to webapps (decent webbrowser etc...)
* Software (flashcard, dictionary etc...)
* Multimedia (audio, video, podcasts etc...)
* Text (note taking, keyboard layout etc...)

This article won't cover the details to install, configure and use software.
Refer to your distribution's and its softwares' documentations.

By the way, everything here was accomplished in
[Archlinux](http://archlinux.org/), but it shouldn't be a problem with any other
distribution.


## Flashcards

There are many different programs out there. There is a list
[here](https://en.wikipedia.org/wiki/List_of_flashcard_software).
In the wikipedia article you will find both native linux softwares as well as 
browser-based apps.

Anyway, I did try some of the browser-based one (such as Memrise and Tinycards),
but I never felt I needed something more than [Anki](https://apps.ankiweb.net/). It has waaay more features 
than I would even care to learn, but it is still pretty customizable.

Anki is actually cross platform and it will work basicaly everywhere.


## Note taking

At some point you will need to take some notes. In fact, it might save your ass
if you introduce the habit of note-taking as soon (and as often) as possible 
to your language learning routines.

If you already have a favorite note taking app, you're lucky. If you are
already familiar with a text editor, it might be worth just to use that.

I used [Zim-wiki-desktop](http://zim-wiki.org/) for years. It is a really nice
way to build your own Wiki. However, at somepoint I didn't have a computer any
more and access to linux machine with Zim was impossible. Zim has a really nice
feature: all files are just `.txt` files. That means that even if you are not
able to use Zim everywhere, you still have access to your files, despite of
your enviroment.

Well, a cross platform text editor was needed. Eventually, I found
[Vim](http://www.viemu.com/a-why-vi-vim.html) and at some
point I met [Neovim](http://neovim.io/) which is my current text editor.

Because of [Jekyll](http://jekyllrb.com/) I learned how to use
[Markdown](https://daringfireball.net/projects/markdown/)
which is a really nice markup language, perfect for keeping cross-platform and
text editor agnostic notes.

My currently setup consits of using Neovim with my own [dumb markdown's
configurations](https://gitlab.com/lsrdg/markdumb.vim)
 and [a dumb note taking plugin](https://gitlab.com/lsrdg/markdumb.vim).

Couldn't be happier. o/

## Translations

10 years ago I was really happy with my setup with
[Stardict](https://en.wikipedia.org/wiki/StarDict), but I am not
what has happened, but I just fail everytime I try to find some dictionary files _for free_. 
It see looks like they have disappeared. If you are looking for a dictionary, my
reasearches points that [GoldenDict](http://goldendict.org/) might be what you
are looking for, but again, I failed to find good free dictionary files.

Since I do have internet connection most of the time (and my laptop doesn't have
battery life to live outside), it hasn't being an issue. My current setup
consists of [Translate Shell](https://www.soimort.org/translate-shell/) that, as
the name suggests, it works from the command line, and quoting theproject's web
page:

> "...Translate Shell (formerly Google Translate CLI) is a command-line translator
> powered by Google Translate (default), Bing Translator, Yandex.Translate and
> Apertium..."

[Tatoeba](http://tatoeba.org/) is a great (and open-source!) project with a
great collection of sentences and translations. It will work on your browser.
However, if you need access to it from the command-line, there's some work going
on at [Tatoeba-karini](tatoeba-karini). What is does, is that it makes possible
to both fetch the content at tatoeba.org and, if you want, it helps you download
and use it at your command line as shown below:

{% highlight bash linenos %}

$ tatoeba-karini -s eng epo water
['2049', 'eng', "What happened? There's water all over the apartment."]
['431649', 'epo', 'Kio okazis? Estas akvo en la tuta apartamento.']

['2049', 'eng', "What happened? There's water all over the apartment."]
['509139', 'epo', 'Kio okazis? Estas akvo ĉie en la apartamento.']

{% endhighlight %}
