---
layout: page
title: "Responsivebp - pt_BR"
category: "mistranslations"
permalink: "/responsivebp-pt/"

img: "/images/responsive.png"
alt: "Responsivebp homepage translated to Portuguese."
---

Responsive is tiny. The combined output CSS and JavaScript is only 20.3kb minified and gzipped but there is a lot of functionality built into the framework with touch, right-to-left language, and accessibility support. It's designed to be dropped-in, as-is to your website such as you would with Normalize.css. Now its documentation is also available in Portuguese. Work in progress.
