---
layout: page
title: Dumbnote.vim
category: "vimscript"
permalink: /dumbnote-vim/

img: "/images/dumbnote.png"
alt: "Dumbnote.vim plugin logo art."
---

It was supposed to be a wiki-style, such as Vimwiki or Zim - A Desktop Wiki. But
nope.

Dumbnote is just a personal Vim plugin for managing notes.

Zim is really great as an organizer, but it is not cross-platform and... well,
it is not Vim.

Vimwiki is a really nice (and huge) project, it offers waaaay more than what
what I need, but since there hasn't been any activities on the project and I was
missing markdown support... decided to take the plunge out.
