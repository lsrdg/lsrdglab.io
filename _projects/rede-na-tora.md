---
layout: page
title: "Rede na tora"
category: "rewebbing"
permalink: "/rede-na-tora/"

img: "/images/redenatora.png"
alt: "Printscreen of redenatora.gitlab.io 's homepage."
---

Notes in Portuguese about a web adventure. One of the first experiences in
tweaking a [Jekyll](http://jekyllrb.com/) template. And a good excuse to keep writing.

![Rede na tora]({{ site.url }}/images/redenatora.png)
## [redenatora.gitlab.io](https://redenatora.gitlab.io)

It used to be my lab, since it was the only website that I was both maintaining
and creating content to.

Rede na tora is built open [BlackDoc](http://karloespiritu.com/blackdoc/):

> _"A two-column black theme with a scrolling sidebar. Ideal for sites requiring a
> master-detail layout, e.g. documentation, cheatsheets, lyrics, notes..."_

I didn't give up about this blog. It actually helped me to see what I wanted to
write about, and consequently, what I should learn (and then be able to write
about). I haven't posted in months, but many different projects are on their
way, and they will certainly find their place at Rede na tora.

The project is hosted on [Gitlab](https://github.com/lsrdg/gitlab.com).
