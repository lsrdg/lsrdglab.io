---
layout: page
title: "About"
permalink: /about/
---

## The blue one!

...you know, after Mars, the little blue one. Once you're staring at it, I come
from that green smudge over there, but you can find me on the opposite
side,close to that white stain there.

If you need a juggler/performance: [eliasrodrigo.com](elias-rodrigo).

If you want to take a look at my projects: see my [portfolio](portfolio).

Do I work on open source projects? For sure! You can find me on
[Github](https://github.com/lsrdg/), but I work mostly on
[Gitlab](https://gitlab.com/lsrdg/).

If all you need is a translation (technical, formal/informal, serious/comedy
etc..) you can [send me an e-mail](contact) or just take a look at some open
source translations I am working on [here](mistranslations).

