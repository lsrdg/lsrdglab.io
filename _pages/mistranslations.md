---
layout: page
title: "Mistranslations"
category: "mistranslations"
permalink: /mistranslations/
---

{% assign current-category = page.category %}
{% for page in site.projects %}
{% if page.category == current-category %}

  <div class="post">
    <h3 class="post-title">
      <a href="{{ page.url }}">
        {{ page.title | escape }}
      </a>
    </h3>
    {{ post.excerpt }}

  </div>
{% endif %}
{% endfor %}
